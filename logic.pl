sentense --> predicate.
sentense --> connective.

connective --> unaryConnective, sentense.
connective --> 
	blockOpen, sentense, binaryConnective, 
	sentense, blockClose.

blockOpen --> ['('].
blockClose --> [')'].

unaryConnective  --> [~].
binaryConnective --> [->].
binaryConnective --> [<->].
binaryConnective --> [and].
binaryConnective --> [or].

predicate --> [a].
predicate --> [b].
predicate --> [c].
predicate --> [d].
predicate --> [e].
predicate --> [f].
